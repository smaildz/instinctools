package dajver.facebooksdksample;

import android.app.Activity;
import android.os.Bundle;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;
import com.facebook.android.Util;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class news_activity extends Activity {

    public static final String APP_ID = String.valueOf(R.string.fb_api_key);
    Facebook mFacebook;
    AsyncFacebookRunner mAsyncRunner;

    ArrayList<String>  listAdress;
    String adress="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        if (APP_ID == null) {
            Util.showAlert(this,
                    "Warning", "Facebook Application ID must be set...");
        }
        mFacebook = new Facebook(APP_ID);
        mAsyncRunner = new AsyncFacebookRunner(mFacebook);


        try {

            final Request request = Request.newGraphPathRequest(
                    Session.getActiveSession(),
                   getString(R.string.fb_api_key),
                    new Request.Callback() {
                        @Override
                        public void onCompleted(Response response) {

                            try {
                                listAdress = new ArrayList<String>();

                             JSONObject js = new JSONObject(response.getRawResponse());
                                JSONArray jsonArray = js.getJSONArray("data");
                               for (int i=0; i<=jsonArray.length(); i++) {
                                   JSONObject data = jsonArray.getJSONObject(i);
                                     adress="";
                                   String s = data.toString();
                                   int pos = s.indexOf(getString(R.string.likeIn));
                                   int posit = s.indexOf(getString(R.string.likeAt));

                                   char[] a = s.toCharArray();
                                   if(pos > 0) {
                                       for (int j = pos+ 10 ; j < a.length-3; j++) {
                                            adress=adress+a[j];

                                       }
                                       listAdress.add(adress);
                                   }
                                   else if(posit>0)
                                   {
                                       for (int j = posit+10; j < a.length-3; j++) {
                                           adress=adress+a[j];
                                       }

                                       listAdress.add(adress);
                                   }
                               }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

            request.executeAsync();
        }

        catch(Exception e) {
            e.printStackTrace();
        }
    }
}