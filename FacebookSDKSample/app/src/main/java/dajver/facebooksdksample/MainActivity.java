package dajver.facebooksdksample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private UiLifecycleHelper uiFbHelper; //позволяет создавать, автоматически открывать, сохраненять и восстановить активный сеанс
    private LoginButton enterByFB ;//кнопка Log in with Facebook/Log out

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        uiFbHelper = new UiLifecycleHelper(this, statusCallback);
        uiFbHelper.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView txtAutoriseResult = (TextView) findViewById(R.id.txtAutoriseResult);//TextView с результатами авторизации
        enterByFB = (LoginButton) findViewById(R.id.fb_login_button);

        List<String> listPermission = Arrays.asList(getString(R.string.permPublicProfile), getString(R.string.permUserPosts));
        enterByFB.setReadPermissions(listPermission);//разрешения на получение информации о пользователе, а так же получение постов пользователя

        //интерфейс обратного вызова, который будет вызываться при изменении текущего пользователя.
        enterByFB.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
           //авторизация пользователя
            @Override
            public void onUserInfoFetched(GraphUser user) {
                if (user != null) {
                    txtAutoriseResult.setText(user.getName());
                    Toast toast = Toast.makeText(MainActivity.this, getString(R.string.welcome) + user.getName(), Toast.LENGTH_SHORT );
                    toast.show();
                    Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                    startActivity(intent);//переход на основную форму MapsActivity
                }
            }
        });
    }
    //Обеспечивает асинхронные уведомления об изменениях состояния сеанса
    private Session.StatusCallback statusCallback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            if (state.isOpened()) {
                Log.d(getString(R.string.facebookSampleActivity), getString(R.string.fbSessionOpen));
            } else if (state.isClosed()) {
                Log.d(getString(R.string.facebookSampleActivity), getString(R.string.fbSessionClose));
            }
        }
    };
    //взаимодействие
    @Override
    public void onResume() {
        super.onResume();
        uiFbHelper.onResume();
    }
    //возобновление сеанса
    @Override
    public void onPause() {
        super.onPause();
        uiFbHelper.onPause();
    }
    //завершение сеанса
    @Override
    public void onDestroy() {
        super.onDestroy();
        uiFbHelper.onDestroy();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiFbHelper.onActivityResult(requestCode, resultCode, data);
    }
    //сохранение состояния
    @Override
    public void onSaveInstanceState(Bundle savedState) {
        super.onSaveInstanceState(savedState);
        uiFbHelper.onSaveInstanceState(savedState);
    }
}