package dajver.facebooksdksample;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import com.facebook.FacebookException;
import com.facebook.UiLifecycleHelper;
import com.facebook.internal.Utility;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.WebDialog;
import java.io.IOException;

public class add_news extends AppCompatActivity {

    private UiLifecycleHelper fbUIHelper;
    EditText editTextName;//поле для названия публикации
    EditText editTextDescription;//поле описания публикации
    EditText editTextUrl;//поле ссылки для публикации
    EditText editTextImage;//поля для картинки
    EditText editTextPrimech;//поле для примечания
    String Name;
    String Descrip;
    String Url;
    String Image;
    String Primech;
    static final int GALLERY_REQUEST = 1;
    ImageButton imgBtnPublishNews; //Button для загрузки изображенрия из галереи
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_news);

        editTextName = (EditText) findViewById(R.id.editTxtName);
        editTextDescription = (EditText) findViewById(R.id.editTxtDescription);
        editTextUrl = (EditText) findViewById(R.id.editTxtUrl);
        editTextImage = (EditText) findViewById(R.id.editTxtImage);
        editTextPrimech = (EditText) findViewById(R.id.editTxtPrimech);
        ImageButton imgBtnAddFoto = (ImageButton)findViewById(R.id.imgBtnFoto);

        imgBtnAddFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType(getString(R.string.image));
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
            }
        });

        fbUIHelper = new UiLifecycleHelper(this, null);
        fbUIHelper.onCreate(savedInstanceState);
        imgBtnPublishNews = (ImageButton) findViewById(R.id.imgBtnPublishNews);

        //нажатие на кнопку "Поделится", обрабатываются все переменные и вызывется метод facebookPublish с 5-ю параметрами
        imgBtnPublishNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    if(editTextName.getText().length()==0){
                        Name = getString(R.string.defaultNameNews);
                    }
                    else {
                        Name=""+editTextName.getText();
                    }

                    if(editTextDescription.getText().length()==0){
                        Descrip=getString(R.string.defaultDescriptionNews);
                    }
                    else {
                        Descrip=""+editTextDescription.getText();
                    }

                    if(editTextPrimech.getText().length() == 0){
                        Primech=getString(R.string.defaultNoteNews);
                    }
                    else {
                        Primech=""+editTextPrimech.getText();
                    }

                    if(editTextUrl.getText().length()==0){
                        Url=getString(R.string.defaultUrlNews);
                    }
                    else {
                        Url=""+editTextUrl.getText();
                    }

                    if(editTextImage.getText().length()==0){
                        Image=getString(R.string.defaultImageUrlNews);
                    }
                    else {
                        Image=""+editTextImage.getText();
                    }

                    facebookPublish(Name, Primech, Descrip, Url, Image);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
//загрузка изоражения из галереии
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

            super.onActivityResult(requestCode, resultCode, data);

        Bitmap bitmap = null;
        ImageView imageView = (ImageView) findViewById(R.id.imageView);

        switch(requestCode) {
            case GALLERY_REQUEST:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = data.getData();
                    imageView.setImageURI(selectedImage);
                    editTextImage.setText("https://"+selectedImage.toString());
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imageView.setImageBitmap(bitmap);
                }
        }
            fbUIHelper.onActivityResult(requestCode, resultCode, data);

        fbUIHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {

                    @Override
                    public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
                        Toast.makeText(add_news.this, getString(R.string.notPublishNews) , Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
                        Toast.makeText(add_news.this, getString(R.string.okPublishNews), Toast.LENGTH_SHORT).show();
                    }

                }

        );
    }

    @Override
    public void onResume() {
        super.onResume();
        fbUIHelper.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        fbUIHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        fbUIHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle savedState) {
        super.onSaveInstanceState(savedState);
        fbUIHelper.onSaveInstanceState(savedState);
    }

    //метод обрабатывающий добавление публикации в facebook
    public final void facebookPublish(String name, String caption, String description, String link, String pictureLink) {
        if (FacebookDialog.canPresentShareDialog(getApplicationContext(), FacebookDialog.ShareDialogFeature.SHARE_DIALOG)) {
            FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(this)
                    .setName(name)
                    .setCaption(caption)
                    .setDescription(description)
                    .setLink(link)
                    .setPicture(pictureLink)
                    .build();
            fbUIHelper.trackPendingDialogCall(shareDialog.present());
        } else {
            Bundle params = new Bundle();
            params.putString(getString(R.string.name), name);
            params.putString(getString(R.string.caption), caption);
            params.putString(getString(R.string.description), description);
            params.putString(getString(R.string.link), link);
            params.putString(getString(R.string.picture), pictureLink);
            WebDialog Dialog = new WebDialog.FeedDialogBuilder(this, Utility.getMetadataApplicationId(this), params)
                    .setOnCompleteListener(new WebDialog.OnCompleteListener() {
                       @Override
                        public void onComplete(Bundle values, FacebookException error) {
                            if ((values != null) && (values.getString(getString(R.string.postId)) != null) && (error == null)) {
                                Toast.makeText(add_news.this, getString(R.string.okPublishNews), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(add_news.this, getString(R.string.notPublishNews), Toast.LENGTH_SHORT).show();
                            }
                            ;
                        }

                        ;
                    })
                    .build();
            Dialog.show();
        }
    }
}
