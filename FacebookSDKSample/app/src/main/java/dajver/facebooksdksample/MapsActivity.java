package dajver.facebooksdksample;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements GoogleMap.OnMarkerClickListener   {

    SupportMapFragment mapFragment;//
    GoogleMap map;//для работы Google Maps
    ImageButton imgBtnAdddNews;//Button для перехода на Activity добавления публикации
    LocationManager locationManager;//класс обеспечивает доступ к сервисам системы определения местоположения
    final String TAG = "myLogs";//для записи в логи
    ImageButton imgBtnMyLocation;//Button для перехода к настройкам вкл/выкл определения местоположения
    LatLng lt;//пара координат широта и долгота
    ImageButton imgBtnUp;//Button обнавляет содержимое Activity
    ArrayList<String> listAdress;//List со списком адресов, постов пользователя
    String adress="";
    //переменные для хранения информации о состоянии местоположения, о провайдере и о статусе
    String tvEnabledGPS;
    String tvStatusGPS;
    String tvLocationGPS;
    String tvEnabledNet;
    String tvStatusNet;
    String tvLocationNet;
    private Marker myMarker;// маркер на карте

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        imgBtnAdddNews = (ImageButton) findViewById(R.id.imgBtnAddNews);
        imgBtnMyLocation = (ImageButton) findViewById(R.id.imgBtnMyLocation);
        imgBtnUp = (ImageButton) findViewById(R.id.imgBtnUpdate);

        //нажатие кнопки обновить
        imgBtnUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                overridePendingTransition(0, 0);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                finish();
                overridePendingTransition(0, 0);
                startActivity(intent);
            }
        });

        //нажатие кнопки imgBtnMyLocation для настроеки определения местоположение
        imgBtnMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(
                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));


            }
        });
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        map = mapFragment.getMap();
        if (map == null) {
            finish();
            return;
        }
        initMaps();

        //при нажатии на карту сатвить маркер
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                setUpMap(latLng, "Marker", getString(R.string.isMarker));
            }
        });
        //кнопки увеличения/уменшения масштаба
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setZoomGesturesEnabled(true);

        //нажатие на кнопку добавить публикацию, перезод на Activity add_news
        imgBtnAdddNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapsActivity.this, add_news.class);
                startActivity(intent);
            }
        });
        //получение постов пользователя
        try {
            final Request request = Request.newGraphPathRequest(
                    Session.getActiveSession(),
                    getString(R.string.my_feed),
                    new Request.Callback() {
                        @Override
                        public void onCompleted(Response response) {
                            try {
                                listAdress = new ArrayList<String>();
                                JSONObject js = new JSONObject(response.getRawResponse());
                                JSONArray jsonArray = js.getJSONArray("data");
                                for (int i=0; i<=jsonArray.length(); i++) {
                                    JSONObject data = jsonArray.getJSONObject(i);
                                    adress="";
                                    String s = data.toString();
                                    int pos = s.indexOf(getString(R.string.likeIn));
                                    int posit = s.indexOf(getString(R.string.likeAt));
                                    char[] a = s.toCharArray();
                                    if(pos > 0) {
                                        for (int j = pos+ 6 ; j < a.length-3; j++) {
                                            adress=adress+a[j];
                                        }
                                        LatLng adr  = getLocationFromAddress(adress);
                                        setUpMap(adr,getString(R.string.isMyFeed), getString(R.string.feedAdress) + adress+ "(" +adr+ ")");
                                        listAdress.add(adress);
                                    }
                                    else if(posit>0)
                                    {
                                        for (int j = posit+6; j < a.length-3; j++) {
                                            adress=adress+a[j];
                                        }
                                        LatLng adr  = getLocationFromAddress(adress); // вызывается метод для нахождения координат по адресу
                                        setUpMap(adr, getString(R.string.isMyFeed), getString(R.string.feedAdress) + adress + "("+adr+")");
                                        listAdress.add(adress);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
            request.executeAsync();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    //получение координат по адресу, с помощью Geocoder, возвращает координаты
    public LatLng getLocationFromAddress(String strAddress) {
        Geocoder coder = new Geocoder(getApplicationContext());
        List<Address> address;
        LatLng adr = null;
        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();
            adr = new LatLng(location.getLatitude(), location.getLongitude() );

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return adr;
    }
    //метод для создания маркеров на карте, принимает координаты, название и описание
    private void setUpMap(LatLng lasLng, String name, String snipped)
    {
        map.setOnMarkerClickListener(this);
        myMarker = map.addMarker(new MarkerOptions()
                .position(lasLng)
                .title("" + name)
                .snippet("" + snipped)
                .draggable(true)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
    }
    @Override
    //обрабатывает нажатие на маркер
    public boolean onMarkerClick(final Marker marker) {
        if (marker.equals(myMarker)) {
            myMarker.getSnippet();
            Toast.makeText(MapsActivity.this, myMarker.getTitle(), Toast.LENGTH_SHORT).show();
            /*Intent intent = new Intent(MapsActivity.this, news_activity.class);
            startActivity(intent);*/
           }
        return false;
    }
    //взаимодействие
    @Override
    protected void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                1000 * 10, 10, locationListener);
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 1000 * 10, 10,
                locationListener);
        checkEnabled();
    }
    //возобнавление сеанса
    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(locationListener);
    }

    //получения уведомлений от LocationManager , когда местоположение изменилось
    private android.location.LocationListener locationListener = new android.location.LocationListener() {
        //Вызывается, когда местоположение изменилось.
        @Override
        public void onLocationChanged(Location location) {
            showLocation(location);
        }
        //Вызывается, когда провайдер отключен пользователем.
        @Override
        public void onProviderDisabled(String provider) {
            checkEnabled();
        }
        //Вызывается, когда поставщик включен пользователем.
        @Override
        public void onProviderEnabled(String provider) {
            checkEnabled();
            showLocation(locationManager.getLastKnownLocation(provider));
        }
       //Вызывается, когда изменяется статус поставщика.
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (provider.equals(LocationManager.GPS_PROVIDER)) {
                tvStatusGPS=("Status: " + String.valueOf(status));
                Toast.makeText(MapsActivity.this, tvStatusGPS, Toast.LENGTH_SHORT).show();
            } else if (provider.equals(LocationManager.NETWORK_PROVIDER)) {
                tvStatusNet=("Status: " + String.valueOf(status));
                Toast.makeText(MapsActivity.this, tvStatusNet, Toast.LENGTH_SHORT).show();
            }
        }
    };
    //информация о состоянии, и местоположении
    private void showLocation(Location location) {
        if (location == null) {
            return;
        }
        if (location.getProvider().equals(LocationManager.GPS_PROVIDER)) {
            tvLocationGPS=(formatLocation(location));
            Toast.makeText(MapsActivity.this, tvLocationGPS, Toast.LENGTH_SHORT).show();
            setUpMap(lt, getString(R.string.myLocat), getString(R.string.isMyLocation));

        } else if (location.getProvider().equals(
                LocationManager.NETWORK_PROVIDER)) {
            tvLocationNet=(formatLocation(location));
            Toast.makeText(MapsActivity.this, tvLocationNet, Toast.LENGTH_SHORT).show();
            lt=new LatLng(location.getLatitude(), location.getLongitude());
            setUpMap(lt, getString(R.string.myLocat), getString(R.string.isMyLocation));
        }
    }
    //определяет формат отображения координат
    private String formatLocation(Location location) {
        if (location == null)
            return "";
        return String.format(
                getString(R.string.formatCoordinat),
                location.getLatitude(), location.getLongitude());
   }
    //получение информации о сотоянии, провайдере
    private void checkEnabled() {
        tvEnabledGPS=(getString(R.string.enableLoc)
                + locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER));
        Toast.makeText(MapsActivity.this, tvEnabledGPS, Toast.LENGTH_SHORT).show();
        tvEnabledNet=(getString(R.string.enableLoc)
                + locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER));
        Toast.makeText(MapsActivity.this, tvEnabledNet, Toast.LENGTH_SHORT).show();
    }

    //точка входа для всех методов, относящихся к карте
   private void initMaps() {
       //сробатывает при нажатии пользователем на карту
            map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Log.d(TAG, getString(R.string.onMapClick) + latLng.latitude + "," + latLng.longitude);
            }
        });

       //срабатывает при длинном нажатии на карту
        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {

            @Override
            public void onMapLongClick(LatLng latLng) {
                Log.d(TAG, getString(R.string.onMapLongClick) + latLng.latitude + "," + latLng.longitude);
            }
        });
       //срабатывает при изменении камеры
        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

            @Override
            public void onCameraChange(CameraPosition camera) {
                Log.d(TAG, getString(R.string.onCameraChange) + camera.target.latitude + "," + camera.target.longitude);
            }
        });
    }

    public  boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    //обработчик меню
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        switch (id)
        {
            case R.id.menuShowMaps://скрыть карту
                map.setMapType(GoogleMap.MAP_TYPE_NONE);
            break;
            case R.id.menuOrdinaryMaps://обычная карта
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.menuSatelliteMaps://спутник
                map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.menuReliefMaps://рельеф
                map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
            case R.id.menuLocalMaps://местная
                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
            case R.id.menuActionClose://выход
                Intent i = new Intent(MapsActivity.this, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                this.finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }



}



